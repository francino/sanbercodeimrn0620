// 1. SOAL CLASS SCORE
class Score {
  constructor(subject, points, email){
    this.subject = subject,
    this.points = points,
    this.email = email
  }

  average(){
    let total = 0
    for(let i=0; i<this.points.length; i++){
      total += this.points[i]
    }
    return (total / this.points.length).toFixed(1)
  }
}

// console.log('// 1. SOAL CLASS SCORE')
// var angka = [9, 7, 6, 9]
// var nilai = new Score('subject', angka, 'sd@gmail.com')
// nilai.average()

// 2. SOAL Create Score'

function viewScores(data, subject) {
  var indexSubject = data[0].indexOf(subject)
  // console.log(indexSubject);
  let scoreArr = []
  let subjectIndex
  switch(subject){
    case "quiz-1":
      subjectIndex = 1
      break;
    case "quiz-2":
      subjectIndex = 2
      break;
    case "quiz-3":
      subjectIndex = 3
      break;
    default:
      break;
  }
  for(let i=1; i<data.length; i++){
    const score = new Score(subject, data[i][subjectIndex], data[i][0]) 
    scoreArr.push(score)
  }
  console.log(scoreArr)
}
const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]
// console.log('\n// 2. SOAL Create Score\n')

viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

function recapScore(data){
  let recapScore = []
  for(let i=1; i<data.length; i++){
    const [email,...nilai] = data[i]
    const score = new Score('Recap', nilai, email)
    const rata2 = score.average()

    let predikat
    if(rata2 > 90){
      predikat = 'honour'
    } else if(rata2 > 80){
      predikat = 'graduate'
    } else if(rata2 > 70){
      predikat = 'participant'
    }

    const template =
      `${i}. Email: ${score.email}
              Rata-rata: ${rata2}
              Predikat: ${predikat}`
    
    console.log(template)
  }
}

recapScore(data)