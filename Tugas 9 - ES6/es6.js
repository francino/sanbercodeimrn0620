// 1. Mengubah fungsi menjadi fungsi arrow

const golden = () => {
  console.log("this is golden!!")
}

console.log('// 1. Mengubah fungsi menjadi fungsi arrow\n')
golden()

// 2. Sederhanakan menjadi Object literal di ES6
const newFunction = {
  fullName(firstName, lastName) {return firstName+' '+lastName}
}

console.log('\n// 2. Sederhanakan menjadi Object literal di ES6\n')
console.log(newFunction.fullName("William", "Imoh")) 

// 3. Destructuring
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject
console.log('\n// 3. Destructuring\n')
console.log(firstName, lastName, destination, occupation)

// 4. Array Spreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]

console.log('\n// 4. Array Spreading\n')
console.log(combined) 

// 5. Template Literals
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
console.log('\n// 5. Template Literals\n')
console.log(before) 