class Animal {
  constructor(name) {
    this.name = name
    this.legs = 4
    this.cold_blooded = false
  }
}
 
class Ape extends Animal {
  constructor(name) {
    super(sheep.cold_blooded)
    this.name = name;
  }
  yell(){
    console.log("Auooo")
  }
}

class Frog extends Animal {
  constructor(name){
    super(sheep.cold_blooded)
    this.name = name
  }
  jump(){
    console.log("hop hop")
  }
}

console.log('// 1. Animal Class')
console.log('Release 0\n')
var sheep = new Animal("shaun");
console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)

console.log('\nRelease 1\n')
var sungokong = new Ape("kera sakti")
sungokong.yell()

var kodok = new Frog("buduk")
kodok.jump()

console.log('\n// 2. Function to Class\n')
class Clock {
  constructor({template}){
    this.template = template
  }

  render(){
    this.date = new Date()
    this.timer = ''
    this.hours = this.date.getHours()
    this.mins = this.date.getMinutes()
    this.secs = this.date.getSeconds()
    if(this.hours < 10) this.hours = '0'+this.hours
    if(this.mins < 10) this.mins = '0'+this.mins
    if(this.secs < 10) this.secs = '0'+this.secs

    var output = this.template
      .replace('h', this.hours)
      .replace('m', this.mins)
      .replace('s', this.secs);
    console.log(output);
  }

  start(){
    this.render()
    this.timer = setInterval(() => this.render(), 1000)
  }

  stop(){
    clearInterval(timer)
  }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  