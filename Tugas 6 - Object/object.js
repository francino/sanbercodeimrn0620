// Soal No. 1 (Array to Object)
console.log('// Soal No. 1 (Array to Object)\n')
console.log('\n// Driver Code\n')
var now = new Date()
var thisYear = now.getFullYear()

function arrayToObject(arr) {
  var obj = {}
  var newObj = {}
  for(let i=0; i<arr.length; i++){
    obj = {
        firstName: arr[i][0],
        lastName: arr[i][1],
        gender: arr[i][2],
        age: (arr[i][3]) ? thisYear - arr[i][3] : 'Invalid Birth Year',
    }
    // arrObject.push(obj)
    newObj[i+1+'. '+obj.firstName+' '+obj.lastName] = obj
  }
  console.log(newObj)
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)

// Soal No. 2 (Shopping Time)
console.log('\n// Soal No. 2 (Shopping Time)\n')

function shoppingTime(memberId, money){
  listPuschased = []
  var products = [['Sepatu Stacattu', 1500000], ['Baju Zoro', 500000], ['Baju H&N', 250000], ['Sweater Uniklooh', 175000], ['Casing Handphone', 50000]]

  if(memberId == '' || !memberId){
    return 'Mohon maaf, toko X hanya berlaku untuk member saja'
  } else if(money < 50000){
    return 'Mohon maaf, uang tidak cukup'
  } else {
    changeMoney = money
    for(let i=0; i<products.length; i++){
      if(changeMoney >= products[i][1]){
        listPuschased.push(products[i][0])
        changeMoney -= products[i][1]
      }
    }
    obj = {
      memberId: memberId,
      money: money,
      listPuschased: listPuschased,
      changeMoney: changeMoney
    }
    return obj
  }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000))
console.log(shoppingTime('', 2475000))
console.log(shoppingTime('234JdhweRxa53', 15000))
console.log(shoppingTime())

// Soal No. 3 (Naik Angkot)
console.log('\nSoal No. 3 (Naik Angkot)\n')
function naikAngkot(listPenumpang){
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  hasil = []
  for(let i=0; i<listPenumpang.length; i++){
    var awal = rute.indexOf(listPenumpang[i][1])
    var akhir = rute.indexOf(listPenumpang[i][2])
    var selisih = akhir-awal
    var tarif = selisih*2000
    obj = {
      penumpang: listPenumpang[i][0],
      dari: listPenumpang[i][1],
      ke: listPenumpang[i][2],
      tarif: tarif
    }
    hasil.push(obj)
  }
  return hasil
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));