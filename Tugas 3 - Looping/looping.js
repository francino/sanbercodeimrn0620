// No. 1 Looping While
function loopingPertama(deret){
  while(deret < 20){
    deret += 2
    console.log(deret+' - I love coding')
  }
}

function loopingKedua(deret){
  while(deret > 0){
    console.log(deret+' - I love coding')
    deret -= 2
  }
}

console.log('No. 1 Looping While\n')
console.log('LOOPING PERTAMA\n')
loopingPertama(0)
console.log('\nLOOPING KEDUA\n')
loopingKedua(20)

// No. 2 Looping menggunakan for
function loopingFor(){
  for(i=1; i<=20; i++){
    if(i%2 != 0 && i%3 == 0){
      console.log(i+ ' - I love coding')
    } else if(i%2 != 0){
      console.log(i+' - Santai')
    } else if(i%2 == 0){
      console.log(i+' - Berkualitas')
    }
  }
}

console.log('\nNo. 2 Looping menggunakan for\n')
loopingFor()

// No. 3 Membuat Persegi Panjang
function persegiPanjang(baris, kolom){
  var i=0
  var j=0
  var persegiPanjang=''
  while(i < baris){
    while(j < kolom){
      persegiPanjang += '#'
      j++
    }
    console.log(persegiPanjang)
    i++
  }
}

console.log('\nNo. 3 Membuat Persegi Panjang\n')
persegiPanjang(4, 8)

// No. 4 Membuat Tangga
function tangga(angka){
  var tangga = ''
  for(var i=1; i<=angka; i++){
    for(var j=0; j<i; j++){
      tangga += '#'
    }
    console.log(tangga)
    tangga = ''
  }
}

console.log('\nNo. 4 Membuat Tangga\n')
tangga(7)

// No. 5 Membuat Papan Catur
function papanCatur(baris, kolom){
  var papanCatur=''
  for(var i=0; i<=baris; i++){
    for(var j=0; j<kolom; j++){
      if(i%2 == 0){
        if(j%2 != 0){
          papanCatur += '#'
        } else {
          papanCatur += ' '
        }
      } else {
          if(j%2 == 0){
            papanCatur += '#'
          } else {
            papanCatur += ' '
          }
      }
      
    }
    console.log(papanCatur)
    papanCatur = ''
  }
}

console.log('\nNo. 5 Membuat Papan Catur\n')
papanCatur(8,8)