// Soal No. 1 (Range) 
function range(num1, num2){
  var range = []
  switch(true){
    case (num1 < num2):
      for(num1; num1<=num2; num1++){
        range.push(num1)
      }
      return range
    break;
    case (!num1 || !num2):
      range.push(-1)
      return range
    break;
    case (num2 < num1):
      for(num1; num2<=num1; num1--){
        range.push(num1)
      }
      return range
    break;
  }
}

console.log('Soal No. 1 (Range)\n')
console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())

// Soal No. 2 (Range with Step)
function rangeWithStep(startNum, finishNum, step){
  var range = []
  switch(true){
    case (startNum < finishNum):
      while (finishNum >= startNum) {
        range.push(startNum);
        startNum += step;
      }
      return range
    break;
    case (finishNum < startNum):
      while (finishNum <= startNum) {
        range.push(startNum);
        startNum -= step;
      }
      return range
    break;
  }
}

console.log('\nSoal No. 2 (Range with Step)\n')
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

// Soal No. 3 (Sum of Range)
function sum(start, finish, step){
  var total = 0
  var angka = []
  if(!start && !finish && !step){
    return total
  } else if(!finish && !step){
    total += start
    return total
  } else if(!step){
    angka = range(start, finish)
    for(i=0; i<angka.length; i++){
      total += angka[i]
    }
    return total
  } else {
    angka = rangeWithStep(start, finish, step)
    for(i=0; i<angka.length; i++){
      total += angka[i]
    }
    return total
  }
}

console.log('\nSoal No. 3 (Sum of Range)\n')
console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15,10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())

// Soal No. 4 (Array Multidimensi)
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
var teks = ["Nomor ID", "Nama Lengkap", "TTL", "Hobi"] 
function dataHandling(){
  item = ''
  hasil =''
  for(var i=0; i<input.length; i++){
    for(var j=0; j<input[i].length; j++){
      if(j == 2){
        item += teks[j]+' : '+input[i][j]
      } else if(j == 3){
        item += ', '+input[i][j]+'\n'
      } else if(j == 4) {
        item += teks[j-1]+' : '+input[i][j]+'\n'
      } else {
        item += teks[j]+' : '+input[i][j]+'\n'
      }
    }
    hasil += item+'\n'
    item = ''
  }
  return hasil
}

console.log('\nSoal No. 4 (Array Multidimensi)\n')
console.log(dataHandling())

// Soal No. 5 (Balik Kata)
function balikKata(kata){
  kataBaru =''
  for(let i=kata.length-1; i>=0; i--){
    kataBaru += kata[i]
  }
  return kataBaru
}

console.log('\nSoal No. 5 (Balik Kata)\n')
console.log(balikKata('Kasur rusaK'))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

// Soal No. 6 (Metode Array)
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]

function dataHandling2(input){
  var teksBulan
  input.splice(1, 1, 'Roman Alamsyah Elsharawy')
  input.splice(2, 1, 'Provinsi Bandar Lampung')
  input.splice(4, 1, 'Pria', 'SMA Internasional Metro')
  console.log(input)
  var pecahTanggal = input[3].split('/')
  switch (true) {
    case pecahTanggal[1] == 1:
      teksBulan = 'Januari'
      break;
    case pecahTanggal[1] == 2:
      teksBulan = 'Februari'
      break;
    case pecahTanggal[1] == 3:
      teksBulan = 'Maret'
      break;
    case pecahTanggal[1] == 4:
      teksBulan = 'April'
      break;
    case pecahTanggal[1] == 5:
      teksBulan = 'Mei'
      break;
    case pecahTanggal[1] == 6:
      teksBulan = 'Juni'
      break;
    case pecahTanggal[1] == 7:
      teksBulan = 'Juli'
      break;
    case pecahTanggal[1] == 8:
      teksBulan = 'Agustus'
      break;
    case pecahTanggal[1] == 9:
      teksBulan = 'September'
      break;
    case pecahTanggal[1] == 10:
      teksBulan = 'Oktober'
      break;
    case pecahTanggal[1] == 11:
      teksBulan = 'November'
      break;
    case pecahTanggal[1] == 12:
      teksBulan = 'Desember'
      break;
    default:
      break;
  }
  console.log(teksBulan)

  var sortTgl = pecahTanggal.sort(function (value1, value2) { return value2 - value1 } )
  console.log(sortTgl)

  var tglBaru = input[3].split('/').join('-')
  console.log(tglBaru)

  var nama = input[1].slice(0, 15);
  console.log(nama)
}

console.log('\nSoal No. 6 (Metode Array)\n')
dataHandling2(input)