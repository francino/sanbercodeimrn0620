// If-else
function soalIfElse(nama, peran){
  if(nama == ''){
    console.log('Nama harus diisi!\n')
  } else if(nama && peran == ''){
    console.log('Halo '+nama+', Pilih peranmu untuk memulai game!\n')
  } else if(nama == 'Jane' && peran == 'Penyihir'){
    console.log('Selamat datang di Dunia Werewolf, Jane\nHalo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!\n')
  }else if(nama == 'Jenita' && peran == 'Guard'){
    console.log('Selamat datang di Dunia Werewolf, Jenita\nHalo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.\n')
  }else if(nama == 'Junaedi' && peran == 'Werewolf'){
    console.log('Selamat datang di Dunia Werewolf, Junaedi\nHalo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!\n')
  }
}

console.log('Soal If-Else 1')
soalIfElse('', '')
console.log('Soal If Else 2')
soalIfElse('John', '')
console.log('Soal If Else 3')
soalIfElse('Jane', 'Penyihir')
console.log('Soal If Else 4')
soalIfElse('Jenita', 'Guard')
console.log('Soal If Else 5')
soalIfElse('Junaedi', 'Werewolf')

console.log('Soal Switch Case')
var tanggal = 3
var bulan = 8
var tahun = 1900
var teksBulan

switch (true) {
  case (tanggal < 1 || tanggal > 31):{
    console.log('Input tanggal salah') 
    break;
  }
  case (tahun < 1900 || tahun > 2200):{
    console.log('Input tahun salah')
    break;
  }
  case (bulan > 12 || bulan < 1):
    console.log('Input bulan salah')
    break;
  default:
    {
      switch (true) {
        case bulan == 1:
          teksBulan = 'Januari'
          break;
        case bulan == 2:
          teksBulan = 'Februari'
          break;
        case bulan == 3:
          teksBulan = 'Maret'
          break;
        case bulan == 4:
          teksBulan = 'April'
          break;
        case bulan == 5:
          teksBulan = 'Mei'
          break;
        case bulan == 6:
          teksBulan = 'Juni'
          break;
        case bulan == 7:
          teksBulan = 'Juli'
          break;
        case bulan == 8:
          teksBulan = 'Agustus'
          break;
        case bulan == 9:
          teksBulan = 'September'
          break;
        case bulan == 10:
          teksBulan = 'Oktober'
          break;
        case bulan == 11:
          teksBulan = 'November'
          break;
        case bulan == 12:
          teksBulan = 'Desember'
          break;
        default:
          break;
      }
      console.log(tanggal+' '+teksBulan+' '+tahun)
      break;
    }
}