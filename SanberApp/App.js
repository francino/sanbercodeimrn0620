
import React, {Component} from 'react';

import LoginScreen from './Tugas13/LoginScreen'
import AboutScreen from './Tugas13/AboutScreen'
import Tugas14 from './Tugas14/App'
import SkillScreen from './Tugas14/SkillScreen'
import TugasNav from './Tugas15'

//Tugas 14
// command class App section untuk menjalankan soal no 1
// command function App section untuk menjalankan soal no 2
// Soal No. 1
// function App() {
//   return (
//       // <LoginScreen/>
//       <Tugas14/>
//   )
// }

//Soal No. 2
// class App extends Component {
//   render() {
//     return (
//         // <LoginScreen/>
//         // <AboutScreen/>
//         <SkillScreen/>
//     )
//   }
// }

class App extends Component {
  render() {
    return (
      <TugasNav/>
    )
  }
}

export default App