import React, {Component} from 'react';
import { StyleSheet, Text, TextInput, View, Image, TouchableOpacity, FlatList, Dimensions, Platform } from 'react-native';

let headerHeight = Platform.OS === 'android' ? 46 : 66;
let footerHeight = 55;

const Constants = {
  headerHeight: headerHeight,
  footerHeight: footerHeight,
  viewHeight: Dimensions.get('window').height - headerHeight,
  screenHeight: Dimensions.get('window').height,
  screenWidth: Dimensions.get('window').width,
};

export default class LoginScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image source={require('./images/logo.png')} style={{width: 375, height: 102}} />
          <Text style={styles.headerText}>Login</Text>
        </View>
        <View style={styles.body}>
          <View style={{marginBottom: 10}}>
            <Text style={styles.LabelTextField}>Username/Email</Text>
            <View style={styles.TextFieldView}>
              <TextInput/>
            </View>
            <Text style={styles.LabelTextField}>Password</Text>
            <View style={styles.TextFieldView}>
              <TextInput/>
            </View>
          </View>
          <View>
          <View style={{justifyContent: 'center', alignItems: 'center',}}>
            <TouchableOpacity style={styles.buttonMasuk}>
              <Text style={{color: '#fff'}}>Masuk</Text>
            </TouchableOpacity>
            <Text style={{color: '#3EC6FF'}}>Atau</Text>
            <TouchableOpacity style={styles.buttonDaftar}>
              <Text style={{color: '#fff'}}>Daftar ?</Text>
            </TouchableOpacity>
          </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    height: 150,
    alignItems: 'center',
    justifyContent: 'center'
  },
  body: {
    flex: 1,
    paddingTop: 20
  },
  headerText: {
    fontSize: 18,
    color: '#003366',
    alignItems: 'center',
    paddingTop: 10
  },
  LabelTextField: {
    marginHorizontal: 40,
    fontSize: 14
  }, 
  TextFieldView: {
    height: Constants.screenHeight * 0.06,
    width: Constants.screenWidth * 0.80,
    marginTop: 5,
    marginBottom: 10,
    borderColor: '#003366',
    borderWidth: 1,
    justifyContent: 'center',
    marginHorizontal: 40
  },
  buttonMasuk: {
    width: 140,
    height: 40,
    borderRadius: 16,
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20,
    backgroundColor: '#3EC6FF',
  },
  buttonDaftar: {
    width: 140,
    height: 40,
    borderRadius: 16,
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20,
    backgroundColor: '#003366',
  },
})