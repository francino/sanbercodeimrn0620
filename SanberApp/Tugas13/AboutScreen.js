import React, {Component} from 'react';
import { StyleSheet, Text, TextInput, View, Image, TouchableOpacity, FlatList, Dimensions, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons'
import IconAnt from 'react-native-vector-icons/AntDesign'

let headerHeight = Platform.OS === 'android' ? 46 : 66;
let footerHeight = 55;

const Constants = {
  headerHeight: headerHeight,
  footerHeight: footerHeight,
  viewHeight: Dimensions.get('window').height - headerHeight,
  screenHeight: Dimensions.get('window').height,
  screenWidth: Dimensions.get('window').width,
};

export default class AboutScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>Tentang Saya</Text>
          <Icon name="account-circle" color='#efefef' size={200} />
        </View>
        <View style={styles.body}>
          <View style={styles.nameTextView}>
            <Text style={styles.nameText}>Francino Gigih</Text>
            <Text style={styles.jobText}>React Native Developer</Text>
          </View>
          <View style={{alignItems:'center'}}>
            <View style={styles.portofolioView}>
              <Text style={styles.contentTitle}>Portofolio</Text>
              <View style={{height:0.5, backgroundColor:'#E5E5E5', marginLeft: 5, marginRight: 5}} />
              <View style={styles.content}>
                <View style={styles.contentItem}>
                  <IconAnt name="gitlab" color='#3EC6FF' size={50}/>
                  <Text style={styles.contentName}>@francino</Text>
                </View>
                <View style={styles.contentItem}>
                  <IconAnt name="github" color='#3EC6FF' size={50}/>
                  <Text style={styles.contentName}>@francino</Text>
                </View>
              </View>
            </View>
            <View style={styles.contactView}>
              <Text style={styles.contentTitle}>Hubungi Saya</Text>
              <View style={{height:0.5, backgroundColor:'#E5E5E5', marginLeft: 5, marginRight: 5}} />
              <View style={styles.contact}>
                <View style={styles.contactItem}>
                  <IconAnt name="facebook-square" color='#3EC6FF' size={50}/>
                  <View style={{height: 50}}>
                    <Text style={styles.contactText}>@francino</Text>
                  </View>
                </View>
                <View style={styles.contactItem}>
                  <IconAnt name="instagram" color='#3EC6FF' size={50}/>
                  <Text style={styles.contactText}>@francino</Text>
                </View>
                <View style={styles.contactItem}>
                  <IconAnt name="twitter" color='#3EC6FF' size={50}/>
                  <Text style={styles.contactText}>@francino</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    height: 300,
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerText: {
    fontSize: 38,
    fontWeight: 'bold',
    color: '#003366',
    alignItems: 'center',
    paddingTop: 10
  },
  body: {
    flex: 1,
  },
  nameTextView: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  nameText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#003366',
    marginBottom: 8
  },
  jobText: {
    fontSize: 16,
    color: '#3EC6FF'
  },
  portofolioView: {
    width: Constants.screenWidth * 0.95,
    height: 140,
    marginTop: 16,
    borderRadius: 16,
    backgroundColor: '#efefef'
  },
  contentTitle: {
    marginTop: 5,
    marginLeft: 8,
    marginBottom: 8,
    fontSize: 18,
    color: '#003366'
  },
  content: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  contentItem: {
    alignItems: 'center',
    justifyContent: 'center'
  }, 
  contentName: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#003366',
    paddingTop: 3
  },
  contactView: {
    width: Constants.screenWidth * 0.95,
    height: 251,
    marginTop: 16,
    borderRadius: 16,
    backgroundColor: '#efefef'
  },
  contact: {
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contactItem: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
  }, 
  contactText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#003366',
    paddingTop: 18,
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
})