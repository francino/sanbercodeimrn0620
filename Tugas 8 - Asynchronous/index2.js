var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var time = 10000
function count(i){
  if(i == books.length){
    return 0
  } else {
    readBooksPromise(time, books[i])
    .then(function(fullfilled){
      time = fullfilled
      count(i+1)
    })
    .catch(function(error){
      return error
    })
  }
}

count(0)