// Soal No. 1
function teriak(){
  return 'Halo Sanbers!'
}

console.log('// Soal No. 1\n')
console.log(teriak())

// Soal No. 2
function kalikan(num1, num2){
  return num1 * num2
}

var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log('\n// Soal No. 2\n')
console.log(hasilKali)

// Soal No. 3
function introduce(name, age, address, hobby){
  return 'Nama saya '+name+', umur saya '+age+' tahun, alamat saya di '+address+', dan saya punya hobby yaitu '+hobby+'!'
}

var name = "Gigih"
var age = 27
var address = "Jln. Raya 88, Semarang"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log('\n// Soal No. 3\n')
console.log(perkenalan)