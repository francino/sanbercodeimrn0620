function AscendingTen(num) {
  var deret = ''
  if(!num){
    return -1
  } else {
    for(let i=0; i<10; i++){
      deret += num+' '
      num++
    }
    return deret
  }
}

function DescendingTen(num) {
  var deret = ''
  if(!num){
    return -1
  } else {
    for(let i=0; i<10; i++){
      deret += num+' '
      num--
    }
    return deret
  }
}

function ConditionalAscDesc(ref, check){
  var deret = ''
  if(!ref || !check){
    return -1
  } else if(check % 2 == 0){
    for(let i=0; i<10; i++){
      deret += ref+' '
      ref--
    }
    return deret
  } else {
    for(let i=0; i<10; i++){
      deret += ref+' '
      ref++
    }
    return deret
  }
}

function ularTangga(){
  var deretAngka = ''
  var angka = 100
  for(var i=0; i<10; i++){
    if(i%2 == 0){
      for(var j=0; j<10; j++){
        deretAngka += angka+' '
        angka--
      }
      angka -= 9
    } else {
      for(var j=0; j<10; j++){
        deretAngka += angka+' '
        angka++
      }
      angka -= 11
    }
    console.log(deretAngka)
    deretAngka = ''
  }
  return deretAngka
}

console.log('// TEST CASES Ascending Ten\n')
console.log(AscendingTen(11))
console.log(AscendingTen(21))
console.log(AscendingTen())

console.log('\n// TEST CASES Descending Ten\n')
console.log(DescendingTen(100))
console.log(DescendingTen(10))
console.log(DescendingTen())

console.log('\n// TEST CASES Conditional Ascending Descending\n')
console.log(ConditionalAscDesc(20, 8))
console.log(ConditionalAscDesc(81, 1))
console.log(ConditionalAscDesc(31))
console.log(ConditionalAscDesc())

console.log('\n// TEST CASE Ular Tangga\n')
console.log(ularTangga()) 
