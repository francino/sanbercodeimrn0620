function balikString(string) {
  newString =''
  for(let i=string.length-1; i>=0; i--){
    newString += string[i]
  }
  return newString
}

function palindrome(string) {
  newString =''
  for(let i=string.length-1; i>=0; i--){
    newString += string[i]
  }
  if(newString == string){
    return true
  } else {
    return false
  }
}

function bandingkan(num1, num2) {
  if(num1 < 0 || num2 < 0 || num1 == num2 || (!num1 && !!num2)){
    return -1
  } else if(!num2){
    return num1
  } else if(num1 > num2){
    return num1
  } else if(num2 > num1){
    return num2
  }
}

console.log('// TEST CASES BalikString\n')
console.log(balikString("abcde"))
console.log(balikString("rusak"))
console.log(balikString("racecar"))
console.log(balikString("haji"))

console.log('\n// TEST CASES Palindrome\n')
console.log(palindrome("kasur rusak"))
console.log(palindrome("haji ijah"))
console.log(palindrome("nabasan"))
console.log(palindrome("nababan"))
console.log(palindrome("jakarta"))

console.log('\n// TEST CASES Bandingkan Angka\n')
console.log(bandingkan(10, 15))
console.log(bandingkan(12, 12))
console.log(bandingkan(-1, 10)) 
console.log(bandingkan(112, 121))
console.log(bandingkan(1))
console.log(bandingkan())
console.log(bandingkan("15", "18"))